package com.mfs.client.im.dao;

public interface TransactionDao extends BaseDao {

	public Object getTransactionByTransId(String mfsTransId);
}
