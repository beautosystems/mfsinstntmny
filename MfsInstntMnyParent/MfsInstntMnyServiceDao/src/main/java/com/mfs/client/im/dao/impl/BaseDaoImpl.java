package com.mfs.client.im.dao.impl;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mfs.client.im.dao.BaseDao;
import com.mfs.client.im.dto.AuthRequestDto;

public class BaseDaoImpl implements BaseDao {

	@Autowired
	private SessionFactory sessionFactory;

	private static final Logger LOGGER = Logger.getLogger(BaseDaoImpl.class);
	
	@Transactional
	public boolean save(Object obj) throws Exception {
		//LOGGER.info("Inside BaseDAO Save");
		int pk=0;
		boolean isSuccess = false;
		try {
			pk = (Integer)this.sessionFactory.getCurrentSession().save(obj);
			isSuccess = true;
		} catch (ConstraintViolationException e) {
			throw new Exception("Duplicate records found"+e);
		} catch (HibernateException e) {
			throw new Exception(e);
		} catch (Exception e) {
			LOGGER.error(""+e);
		}
		return isSuccess;
	}

	@Transactional
	public boolean update(Object obj) throws Exception {
		LOGGER.debug("Inside BaseDAO Update");
		boolean isSuccess = false;
		try {
			this.sessionFactory.getCurrentSession().update(obj);
			isSuccess = true;
		} catch (HibernateException e) {
			throw new HibernateException(e);
		} catch (Exception e) {
			LOGGER.error(""+e);
		}
		return isSuccess;
	}

	@Transactional
	public boolean saveOrUpdate(Object obj) throws Exception {
		LOGGER.debug("Inside BaseDAO saveOrUpdate");
		boolean isSuccess = false;
		try {
			this.sessionFactory.getCurrentSession().saveOrUpdate(obj);
			isSuccess = true;
		} catch (ConstraintViolationException e) {
			throw new Exception(e);
		} catch (HibernateException e) {

			throw new Exception(e);
		} catch (Exception e) {
			LOGGER.error(""+e);
		}
		return isSuccess;
	}

	@Transactional
	public boolean delete(Object obj) throws Exception {
		LOGGER.debug("Inside BaseDAO delete");
		boolean isSuccess = false;
		try {
			this.sessionFactory.getCurrentSession().delete(obj);
			isSuccess = true;
		} catch (HibernateException e) {
			throw new Exception(e);
		} catch (Exception e) {
			LOGGER.error(""+e);
		}
		return isSuccess;
	}

}