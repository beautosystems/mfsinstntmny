package com.mfs.client.im.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import com.mfs.client.im.dao.TransactionDao;
import com.mfs.client.im.model.ImTransactionLog;


@Repository("TransactionDao")
@EnableTransactionManagement
public class TransactionDaoImpl extends BaseDaoImpl implements TransactionDao {

	@Autowired 
	SessionFactory sessionFactory;
	
	@Transactional
	public Object getTransactionByTransId(String mfsTransId) {
		
		Session session = sessionFactory.getCurrentSession();
		String hql = " From ImTransactionLog where mfsTransId = :mfsTransId";
		Query query = session.createQuery(hql).setParameter("mfsTransId", mfsTransId);

		List<ImTransactionLog> imTransactionLogList = (List<ImTransactionLog>) query.list();
		
		ImTransactionLog imTransactionLog = null;
		if(imTransactionLogList != null && !imTransactionLogList.isEmpty()) {
			imTransactionLog = imTransactionLogList.get(0);
		}
		
		return imTransactionLog;
		
	}
	
	
}


