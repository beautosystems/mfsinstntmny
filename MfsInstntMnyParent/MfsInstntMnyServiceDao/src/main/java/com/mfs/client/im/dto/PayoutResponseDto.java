package com.mfs.client.im.dto;

public class PayoutResponseDto {
	
	private String code;
	
	private String zeepay_id;
	
	private double amount;
	
	private String message;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getZeepay_id() {
		return zeepay_id;
	}

	public void setZeepay_id(String zeepay_id) {
		this.zeepay_id = zeepay_id;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "PayoutResponseDto [code=" + code + ", zeepay_id=" + zeepay_id + ", amount=" + amount + ", message="
				+ message + "]";
	}
	
	
	
	

}
