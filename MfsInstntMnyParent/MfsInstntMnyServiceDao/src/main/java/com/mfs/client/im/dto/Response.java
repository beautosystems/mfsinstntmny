package com.mfs.client.im.dto;

public class Response {
	
	private String registration_status;
	
	private String registration_name;

	public String getRegistration_status() {
		return registration_status;
	}

	public void setRegistration_status(String registration_status) {
		this.registration_status = registration_status;
	}

	public String getRegistration_name() {
		return registration_name;
	}

	public void setRegistration_name(String registration_name) {
		this.registration_name = registration_name;
	}

	@Override
	public String toString() {
		return "Response [registration_status=" + registration_status + ", registration_name=" + registration_name
				+ "]";
	}
	
	

}
