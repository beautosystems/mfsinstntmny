package com.mfs.client.im.dto;

public class ValidateMobileResponseDto {
	
	private String code;

	private Response reponse;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Response getReponse() {
		return reponse;
	}

	public void setReponse(Response reponse) {
		this.reponse = reponse;
	}

	@Override
	public String toString() {
		return "ValidateMobileResponseDto [code=" + code + ", reponse=" + reponse + "]";
	}
	
	
	
	

}
