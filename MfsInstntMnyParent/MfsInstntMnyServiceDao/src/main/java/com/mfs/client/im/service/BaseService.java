package com.mfs.client.im.service;

import java.util.Date;

public interface BaseService {

	
	public void logTransSession(String sender_first_name,String sender_last_name,String receiver_last_name,String receiver_msisdn,String mno
			,String mfs_trans_id,String service_type,String routing_number,String account_number,String third_party_trans_id,String message,
			String status,String amount_sent,String amount_payout,String date_logged);
	
	

	
}
