package com.mfs.client.im.service;

import com.mfs.client.im.dto.TransactionDetailsResponseDto;

public interface GetTransactionService {

	
	
	public TransactionDetailsResponseDto getTransactionDetails();
}
