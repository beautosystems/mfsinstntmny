package com.mfs.client.im.service;

import com.mfs.client.im.dto.ValidateMobileRequestDto;
import com.mfs.client.im.dto.ValidateMobileResponseDto;

public interface ValidateService {
	
	public ValidateMobileResponseDto validate(ValidateMobileRequestDto request);

}
