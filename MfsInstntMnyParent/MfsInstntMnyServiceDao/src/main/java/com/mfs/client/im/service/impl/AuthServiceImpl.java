package com.mfs.client.im.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.mfs.client.im.dao.TransactionDao;
import com.mfs.client.im.dto.AuthRequestDto;
import com.mfs.client.im.dto.AuthResponseDto;
import com.mfs.client.im.service.AuthService;
import com.mfs.client.im.util.CallServices;
import com.mfs.client.im.util.CommonConstant;

@Service("AuthService")
public class AuthServiceImpl extends BaseServiceImpl implements AuthService {

	@Autowired
	TransactionDao transactioDao;
	
	public AuthResponseDto getAccessToken(AuthRequestDto request) {
		System.out.println("in auth serviceImpl");
		
    
		/*String url=CommonConstant.base_url+CommonConstant.Auth_url;
		
		System.out.println("url "+url);*/
		Gson gson = new Gson();
		
		try {
			CallServices.getResponseFromService(CommonConstant.base_url+CommonConstant.Auth_url,gson.toJson(request));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}

	

}
