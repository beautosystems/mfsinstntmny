package com.mfs.client.im.service.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import com.mfs.client.im.dao.TransactionDao;
import com.mfs.client.im.dto.PayoutRequestDto;
import com.mfs.client.im.dto.PayoutResponseDto;
import com.mfs.client.im.model.ImTransactionError;
import com.mfs.client.im.model.ImTransactionLog;
import com.mfs.client.im.service.PayoutService;
import com.mfs.client.im.util.CallServices;
import com.mfs.client.im.util.CommonConstant;
import com.mfs.client.im.util.MFSIMCode;
import com.mfs.client.util.JSONToObjectConversion;

@Service("PayoutService")
public class PayoutServiceImpl extends BaseServiceImpl implements PayoutService {
	
	private static final Logger LOGGER = Logger.getLogger(PayoutServiceImpl.class);
	
	@Autowired
	TransactionDao transactionDao;

	public PayoutResponseDto payout(PayoutRequestDto request) {
		
		LOGGER.info("==>In PayoutServiceImpl in payout function payoutRequest =" + request);
		
		String serviceResponse;
		Gson gson = new Gson();
		PayoutResponseDto response = null;
				
		//Check MFS Transaction Id already processed
		ImTransactionLog imTransactionLog =  (ImTransactionLog) transactionDao.getTransactionByTransId(request.getExtr_id());
				
		if(imTransactionLog != null) 
		{
			response = new PayoutResponseDto();
			response.setCode(MFSIMCode.IMCODE_ERROR_TNX_DUPLICATE.getCode());
			response.setMessage(MFSIMCode.IMCODE_ERROR_TNX_DUPLICATE.getMessage());
			return response;
		}
		
		if(request != null)
		{
			logPayoutTransaction(request);
		}
		
		try {
			LOGGER.info("==> Payout Request : " + gson.toJson(request));
			//LOGGER.info("==> Sending Request to Url : " + fmSystemConfigMap.get(Constant.baseUrl)+ fmSystemConfigMap.get(Constant.NEW_TRANSACTION_URL_CONFIG_KEY));
			
			serviceResponse = CallServices.getResponseFromService(CommonConstant.base_url+CommonConstant.payout_url, gson.toJson(request));
			
			response= (PayoutResponseDto)JSONToObjectConversion.getObjectFromJson(serviceResponse, PayoutResponseDto.class);
			
			if(response != null)
			{
				updatePayoutTransaction(response, request.getExtr_id());
			}
			else
			{
				updatePayoutTransaction(response, request.getExtr_id());
				logPayoutErrorTransaction(request);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return response;
	}

	public ImTransactionLog logPayoutTransaction(PayoutRequestDto request)
	{
		ImTransactionLog logModel = new ImTransactionLog();
		
		logModel.setSenderFirstName(request.getSender_first_name());
		logModel.setSenderLastName(request.getSender_last_name());
		logModel.setSenderCountry(request.getSender_country());
		logModel.setReceiverFirstName(request.getReceiver_first_name());
		logModel.setReceiverLastName(request.getReceiver_last_name());
		logModel.setReceiverMsisdn(request.getReceiver_msisdn());
		logModel.setReceiverCountry(request.getReceiver_country());
		logModel.setAmountSent(request.getAmount());
		logModel.setAddress(request.getAddress());
		logModel.setReceiverCurrency(request.getReceiver_currency());
		logModel.setMno(request.getMno());
		logModel.setTransactionType(request.getTransaction_type());
		logModel.setMfsTransId(request.getExtr_id());
		logModel.setServiceType(request.getService_type());
		logModel.setRoutingNumber(request.getRouting_number());
		logModel.setAccountNumber(request.getAccount_number());
		logModel.setStatus("Initiating");
		logModel.setMessage("Payout transaction initiated");
		
		try {
			transactionDao.save(logModel);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return logModel;	
	}
	
	public void updatePayoutTransaction(PayoutResponseDto response, String mfsTransId)
	{
		ImTransactionLog logModel = (ImTransactionLog) transactionDao.getTransactionByTransId(mfsTransId);
		
		if(logModel != null)
		{
			if(response != null)
			{
				logModel.setStatus("Payout Completed!!");
				logModel.setCode(response.getCode());
				logModel.setMessage(response.getMessage());
				logModel.setThirdPartyTransId(response.getZeepay_id());
				logModel.setAmountSent(response.getAmount());
			}
			else
			{
				logModel.setStatus("Error while getting response");
			}
		
			try {
				transactionDao.update(logModel);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public ImTransactionError logPayoutErrorTransaction(PayoutRequestDto request)
	{
		ImTransactionError errModel = new ImTransactionError();
		
		errModel.setSenderFirstName(request.getSender_first_name());
		errModel.setSenderLastName(request.getSender_last_name());
		errModel.setSenderCountry(request.getSender_country());
		errModel.setReceiverFirstName(request.getReceiver_first_name());
		errModel.setReceiverLastName(request.getReceiver_last_name());
		errModel.setReceiverMsisdn(request.getReceiver_msisdn());
		errModel.setReceiverCountry(request.getReceiver_country());
		errModel.setAmountSent(request.getAmount());
		errModel.setMno(request.getMno());
		errModel.setMfsTransId(request.getExtr_id());
		errModel.setServiceType(request.getService_type());
		errModel.setRoutingNumber(request.getRouting_number());
		errModel.setAccountNumber(request.getAccount_number());
		errModel.setErrorCode(MFSIMCode.ER203.getCode());
		errModel.setErrorMessage(MFSIMCode.ER203.getMessage());
		
		return errModel;
		
	}
	
}
