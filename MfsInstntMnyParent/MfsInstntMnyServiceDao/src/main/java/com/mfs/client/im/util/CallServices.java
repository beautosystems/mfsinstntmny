package com.mfs.client.im.util;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.mfs.client.util.HttpConnectorImpl;
import com.mfs.client.util.HttpsConnectionRequest;
import com.mfs.client.util.HttpsConnectionResponse;
import com.mfs.client.util.HttpsConnectorImpl;


public class CallServices {
	
	private static final Logger LOGGER = Logger.getLogger(CallServices.class);
	
	public static String getResponseFromService(String url, String request) throws Exception {

		HttpsConnectionResponse httpsConResult = null;
		
		String str_result = null;
		
		System.out.println("url "+url);
		System.out.println("data "+request);
		
		/*LOGGER.info("Request Body =>");
		LOGGER.info(request);*/
		try {
			HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
			
			Map<String, String> headerMap = new HashMap<String, String>();

			headerMap.put(CommonConstant.CONTENT_TYPE, CommonConstant.APPLICATION_JSON);

			LOGGER.debug("URL : "+url);
			connectionRequest.setServiceUrl(url);
			connectionRequest.setHeaders(headerMap);
			connectionRequest.setHttpmethodName("POST");
			boolean isHttps = true;
			if (isHttps) {
				connectionRequest.setPort(8443);
				HttpsConnectorImpl httpsConnectorImpl = new HttpsConnectorImpl();
				httpsConResult = httpsConnectorImpl.connectionUsingHTTPS(request, connectionRequest);
			} else {
				HttpConnectorImpl httpConnectorImpl = new HttpConnectorImpl();
				httpsConResult=httpConnectorImpl.httpUrlConnection(connectionRequest,request);
			}
			str_result=httpsConResult.getResponseData();
			
			/*LOGGER.info("Response Body =>");
			LOGGER.info(str_result);*/
		} catch (Exception e) {			
			LOGGER.error(e);
			throw new Exception(e);
		}
		return str_result;
	}
}
