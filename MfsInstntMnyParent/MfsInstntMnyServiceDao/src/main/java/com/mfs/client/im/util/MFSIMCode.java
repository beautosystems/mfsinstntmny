package com.mfs.client.im.util;

public enum MFSIMCode {
	
	IMCODE103("103","Wallet Balance Ceiling Breach"),
	IMCODE403("403","Invalid/Non-existant wallet"),
	IMCODE419("419","Sanctions/OFAC list breach"),
	IMCODE402("402","Client payment required"),
	IMCODE200("200","Validated Successfully"),
	
	VALIDATION_ERROR("ER201","Validation Error"),
	IMCODE_ERROR_TIMEOUT("ER202","Connection Timeout"),
	ER203("ER203","Exception while processing your request, Please try again later."),
	IMCODE_ERROR_TNX_DUPLICATE("ER204","Duplicate Transaction.");
	
    private String code;
	private String message;

	private MFSIMCode(String code, String message) {
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}


}
