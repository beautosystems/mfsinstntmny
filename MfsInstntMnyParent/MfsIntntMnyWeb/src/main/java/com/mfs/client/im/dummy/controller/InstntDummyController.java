package com.mfs.client.im.dummy.controller;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mfs.client.im.dto.AuthRequestDto;
import com.mfs.client.im.dto.AuthResponseDto;
import com.mfs.client.im.dto.PayoutRequestDto;
import com.mfs.client.im.dto.PayoutResponseDto;
import com.mfs.client.im.dto.Response;
import com.mfs.client.im.dto.ValidateMobileRequestDto;
import com.mfs.client.im.dto.ValidateMobileResponseDto;

@RestController
public class InstntDummyController {
	
	@RequestMapping("/oauth/token")
	public AuthResponseDto authDummyCntrl(@RequestBody AuthRequestDto request) {
		
		System.out.println("in dummyc");

		return null;

	}

	@RequestMapping("/api/payout")
	public PayoutResponseDto payoutDummyCntrl(@RequestBody PayoutRequestDto request) {
		
		PayoutResponseDto res =new PayoutResponseDto();
		res.setCode("C200");
		res.setAmount(500.0);
		res.setZeepay_id("DTrxn101");
		res.setMessage("Payout Success");

		return res;

	}
	
	@RequestMapping("/payouts/account-verificationt")
	public ValidateMobileResponseDto payoutDummyCntrl(@RequestBody ValidateMobileRequestDto request) {

		ValidateMobileResponseDto res = new ValidateMobileResponseDto();
		Response resp = new Response();

		res.setCode("200");
		res.setReponse(resp);
		resp.setRegistration_name("jyotiba bhosale");
		resp.setRegistration_status("success");

		return res;
	}





}
