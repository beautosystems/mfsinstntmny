package com.mfs.client.im.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mfs.client.im.dto.AuthRequestDto;
import com.mfs.client.im.dto.AuthResponseDto;
import com.mfs.client.im.dto.PayoutRequestDto;
import com.mfs.client.im.dto.PayoutResponseDto;
import com.mfs.client.im.dto.TransactionDetailsResponseDto;
import com.mfs.client.im.dto.ValidateMobileRequestDto;
import com.mfs.client.im.dto.ValidateMobileResponseDto;
import com.mfs.client.im.service.AuthService;
import com.mfs.client.im.service.GetTransactionService;
import com.mfs.client.im.service.PayoutService;
import com.mfs.client.im.service.ValidateService;

@RestController
public class InstntController {

	@Autowired
	AuthService authservice;

	@Autowired
	PayoutService payoutservice;

	@Autowired
	ValidateService validateservice;

	@Autowired
	GetTransactionService gettransactionservice;

	@RequestMapping("/u")
	public String m1() {

		return "success";
	}

	@RequestMapping("/auth")
	public AuthResponseDto authCntrl(@RequestBody AuthRequestDto request) {
		
		System.out.println("in authCntrl = " + request);
		authservice.getAccessToken(request);

		return null;

	}

	@RequestMapping("/payout")
	public PayoutResponseDto payoutCntrl(@RequestBody PayoutRequestDto request) {

		payoutservice.payout(request);
		return null;

	}

	@RequestMapping("/validateMobileWallet")
	public ValidateMobileResponseDto validateMobileWalletCntrl(@RequestBody ValidateMobileRequestDto request) {
		validateservice.validate(request);

		return null;
	}

	@RequestMapping("/getTransactionStatus")
	public TransactionDetailsResponseDto getTransactionStatusCntrl() {
		gettransactionservice.getTransactionDetails();

		return null;
	}

}
